<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;
use Response;
use Symfony\Component\Intl\Currencies;

class DashboardController extends Controller
{
    /**
     * @param  Request  $request
     * @return mixed
     */
    public function index(Request $request){
        try{
            $shop = \Auth::user();
            $query = $this->orders($request);
            $result = $this->request($query);
            $orders = $result['body']->container['data']['orders']['edges'];

            $after = end($result['body']->container['data']['orders']['edges'])['cursor'] ?? null;
            $before = $result['body']->container['data']['orders']['edges'][0]['cursor'] ?? null;

            if ($orders) {
                foreach ($orders as $pk => $pv) {
                    $node = $pv['node'];
                    $order[$pk]['id'] = $node['legacyResourceId'];
                    $order[$pk]['name'] = $node['name'];
                    $order[$pk]['price'] = Currencies::getSymbol($node['totalPriceSet']['shopMoney']['currencyCode']) . $node['totalPriceSet']['shopMoney']['amount'];
                    $order[$pk]['createdAt'] = date("d M", strtotime($node['createdAt'])) . ' at ' . date("H:i a", strtotime($node['createdAt']));
                    $order[$pk]['customer'] = (@$node['customer']['displayName']) ? $node['customer']['displayName'] : 'No customer';
                }
            } else {
                $order = '';
            }

            $data['order'] = $order;
            $data['pageInfo'] = $result['body']->container['data']['orders']['pageInfo'];
            $data['next'] = $after;
            $data['prev'] = $before;
            return response::json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    public function getOrder(Request $request){
        try{
            $shop = \Auth::user();
            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/orders/'.$request->id.'.json';
            $parameter['fields'] = "id,name,note,line_items,customer,shipping_address,billing_address";
            $result = $shop->api()->rest('GET', $endPoint, $parameter);
            if( !$result['errors'] ){
                $sh_order = $result['body']->container['order'];

                $lineItems = $sh_order['line_items'];
                $lineItem = [];
                if( !empty( $lineItems ) && is_array( $lineItems )){
                    foreach ( $lineItems as $lkey=>$lval ){
                        if( $lval['product_id'] ){
                            $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/products/'. $lval['product_id'] .'.json';
                            $parameter['fields'] = "id,product_type";

                            $products = $shop->api()->rest('GET', $endPoint, $parameter);

                            if( !$products['errors'] ){
                                if ($products['body']->container['product']) {
                                    $product = $products['body']->container['product'];
                                    $product_type = $product['product_type'];

                                    $sh_variant_result = $this->getShopifyVariant($lval['variant_id']);
                                    $l['image'] = ( @$sh_variant_result['body']->container['data']['productVariant']['image']['originalSrc'] ) ? $sh_variant_result['body']->container['data']['productVariant']['image']['originalSrc'] : asset('images/static_upload/no-image-box.png');
                                    $l['name'] = $lval['name'];
                                    $l['productType'] = $product_type;

                                    $price = $lval['price_set']['shop_money'];
                                    $l['price'] = Currencies::getSymbol($price['currency_code']) . $price['amount'];
                                    $l['qty'] = $lval['quantity'];
                                    $l['total'] = Currencies::getSymbol($price['currency_code']) . $lval['price'];

                                    $lineItem[] = $l;
                                }
                            }
                        }
                    }
                }
            }
            $columns = array_column($lineItem, 'productType');
            array_multisort($columns, SORT_DESC, $lineItem);

            $customer = [];
            if( @$sh_order['customer'] ){
                $customer['id'] = ($sh_order['customer']['id']) ? $sh_order['customer']['id'] : '';
                $customer['name'] = ($sh_order['customer']['first_name']) ? $sh_order['customer']['first_name'] . ' ' . $sh_order['customer']['last_name'] : '';
                $customer['total_orders'] = ($sh_order['customer']['orders_count']) ? $sh_order['customer']['orders_count'] : 0;
                $customer['email'] = ($sh_order['customer']['email']) ? $sh_order['customer']['email'] : 'No email address';
                $customer['phone'] = ($sh_order['customer']['phone']) ? $sh_order['customer']['phone'] : 'No phone number';
            }


            $bill_add = 'No shipping address provided';
            if( @$sh_order['billing_address'] ){
                if( strcmp(implode(',', $sh_order['shipping_address']), implode(',', $sh_order['billing_address'])) == 0 ){
                    $bill_add = 'Same as shipping address';
                }else{
                    $bill_add = $sh_order['billing_address'];
                }
            }

            $data['lineItem'] = $lineItem;
            $data['note'] = ($sh_order['note']) ? $sh_order['note'] : 'No notes from customer';
            $data['customer'] = (!empty($customer)) ? $customer : 'No customer';
            $data['shipping_add'] = (@$sh_order['shipping_address']) ? $sh_order['shipping_address'] : 'No shipping address provided';
            $data['billing_add'] = $bill_add;
            return response::json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param $variant_id
     * @param $user
     * @return mixed
     * @throws \Exception
     */
    public function getShopifyVariant($variant_id)
    {
        $query['parameter'] = [];
        $query['query'] = '{
                  productVariant(id: "gid://shopify/ProductVariant/'.$variant_id.'") {
                    image {
                      originalSrc
                    }
                  }
                }';
        $res = $this->request($query);
        return $res;
    }

    /**
     * @return string
     */
    public function orders($request)
    {
        try {
            $limit = 10;
            $after = @($request['after']) ? '"' . $request['after'] . '"' : null;
            $before = @($request['before']) ? '"' . $request['before'] . '"' : null;
            $page = ( $before == null ) ? 'after:' : 'before:';
            $cursor = ( $after == null ) ? $before : $after;

            $s =  ($request->s) ? '"' . $request->s .'*"' : '""';

            $dd = ( $page == 'before:' ) ? 'last' : 'first';
            $page = ( empty($cursor) ) ? '' : $page;

            $query =  '{
                         orders('. $dd .': ' . $limit . ' query: '. $s .' '. $page.$cursor.' reverse: true) {
                            edges {
                              cursor
                              node {
                                legacyResourceId
                                name
                                createdAt
                                customer {
                                  displayName
                                }
                                totalPriceSet {
                                  shopMoney {
                                    amount
                                    currencyCode
                                  }
                                }
                              }
                            }
                            pageInfo {
                              hasNextPage
                              hasPreviousPage
                            }
                          }
                        }';

            $parameters = ['limit' => $limit];
            if ($after) {
                $parameters['after'] = $after;
            }
            if ($before) {
                $parameters['before'] = $before;
            }

            $res['query'] = $query;
            $res['parameter'] = $parameters;
            return $res;
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function orderDetail($id)
    {
        try {
            return '{
                          order(id: "gid://shopify/Order/'.$id.'") {
                            displayFulfillmentStatus
                             legacyResourceId
                                name
                          }
                        }';
        } catch (\Exception $e) {
            dd($e);
        }
    }
    /**
     * @param $query
     * @return array|\GuzzleHttp\Promise\Promise
     * @throws \Exception
     */
    public function request($query)
    {
        $shop = \Auth::user();
        $options = new Options();
        $options->setVersion(env('SHOPIFY_API_VERSION'));
        $api = new BasicShopifyAPI($options);
        $api->setSession(new Session(
            $shop->name, $shop->password));
        return $api->graph($query['query'], $query['parameter']);
    }
}
